package oop.lab1;

//TODO Write class Javadoc
/**
 * A simple model for a Student with name and id
 * 
 * @author Kasdit Phoncharoen
 */
public class Student extends Person {
	/**the student's id*/
	private long id;
	
	//TODO Write constructor Javadoc
	/**
	 * Initialize a new Student object
	 * @param name and id are the name and id of the new student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	/**
	 * Compare student's by id.
	 * They are equal if the id matches.
	 * @param obj is another Objectr to compare to this one.
	 * @return true if the id is same, false otherwise.
	 */
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(obj.getClass() != this.getClass()) 
			return false;
		Student other = (Student) obj; 
		if(id == other.id) 
			return true;
		return false;
	}
}
